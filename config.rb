# Require any additional compass plugins here.
css_dir = "./public/css"
sass_dir = "./sass"
# Set this to the root of your project when deployed:
http_path = "/"
# To enable relative paths to assets via compass helper functions. Uncomment:
output_style = :compact