var app = angular.module('iTook', ['btford.socket-io', 'googlechart']),
	jsonFile = 'data/phones.json';

app.factory('mySocket', function (socketFactory) {
	return socketFactory();
}).controller('phoneController', function($scope, $http, $timeout, mySocket) {

	$scope.usernameDefault = 'wie ben je?';
	$scope.locationDefault = 'waar zit je?';

	$scope.username = (getCookie('username')) ? getCookie('username') : $scope.usernameDefault;
	$scope.location = (getCookie('location')) ? getCookie('location') : $scope.locationDefault;
	$scope.show = ($scope.username == $scope.usernameDefault) ? 'preferences':'';

	$scope.getPhones = function() {
		$http.get(jsonFile).then(function(data) {
			$scope.phones = data.data.phones;
			$scope.updateStats();
		});
	};

	$scope.savePhones = function() {
		$http.post('/save', $scope.phones).then(function(response) {
			$scope.showMsg(response.data);
			$scope.updateStats();
		});
	};

	$scope.takePhone = function (id) {
		var index = id - 1;
		$scope.phones[index].status = "taken";
		$scope.phones[index].takenBy.unshift({
			"name": $scope.username,
			"location": ($scope.location != $scope.locationDefault) ? $scope.location : '',
			"timestamp_out": Date.now(),
			"cable": $scope.phones[index].cable
		});
		$scope.savePhones();
	};

	$scope.returnPhone = function (id) {
		var index = id - 1;
		$scope.phones[index].status = "available";
		$scope.phones[index].cable = "available";
		$scope.phones[index].takenBy[0].timestamp_in = Date.now();
		$scope.savePhones();
	};

	$scope.savePreferences = function() {
		if ($scope.username != $scope.usernameDefault)
			createCookie('username', $scope.username, 9999);
		if ($scope.location != $scope.locationDefault)
			createCookie('location', $scope.location, 9999);
		$scope.showMsg("voorkeuren opgeslagen!");
	};

	$scope.showPanel = function (panel) {
		if (panel != $scope.show) $scope.show = panel;
		else $scope.show = '';
	};

	$scope.clear = function (e) {
		if ($scope.usernameDefault == e.target.value || $scope.locationDefault == e.target.value) e.target.value = '';
	};

	$scope.showMsg = function (msg) {
		$scope.msg = msg;
		$scope.show = '';
		
		$timeout(function () {
			$scope.msg = '';
		}, 3000);
	};
	
	$scope.formatTime = function (timestamp) {
		if (timestamp){
			time = moment(timestamp);
			if (time.isSame(new Date(), 'day'))
				return time.format('HH:mm');
			else
				return time.format('DD/MM/YY HH:mm');
		}
	};

	$scope.updateStats = function () {
		$scope.totalDevices = ($scope.phones !== undefined) ? $scope.phones.length : 0;
		$scope.setTotalStats();
	};

	$scope.setTotalStats = function () {
		var users = {};
		for (var i = 0; i < $scope.phones.length; i++) {
			for (var j = 0; j < $scope.phones[i].takenBy.length; j++) {
				if (users[$scope.phones[i].takenBy[j].name])
					users[$scope.phones[i].takenBy[j].name]++;
				else
					users[$scope.phones[i].takenBy[j].name] = 1;
			}
		}

		$scope.totalTesters = Object.keys(users).length;
		$scope.totalTooks = sum(users);

		$scope.users = [];
		for (var user in users){
			$scope.users.push([user, users[user]]);
		}
		$scope.users = sortObject(users);
		$scope.users = $scope.users.slice(0,5);
	};

	function sum( obj ) {
		var r = 0;
		for ( var el in obj ) {
			if( obj.hasOwnProperty( el ) ) {
				r += parseFloat( obj[el] );
			}
		}
		return r;
	}

	$scope.chartObject = {
		"type": "AreaChart",
		"displayed": true,
		"data": {
			"cols": [{
				"label": "maand",
				"type": "string"
			},
			{
				"label": "uitgeleend",
				"type": "number"
			},{
				"label": "locaties",
				"type": "number"
			},{
				"label": "personen",
				"type": "number"
			}],
			"rows": []
		},
		"options": {
			"fill": 20,
			"aggregationTarget": "series",
			"displayExactValues": true,
			"legend": {
				"position": "top",
				"maxLines": 2,
				"textStyle": {
					"color": "white"
				}
			},
			"backgroundColor": "transparent",
			"colors": [
				"#FF8A71", "white", "#72D0F4"
			],
			"is3D": "true",
			"vAxis": {
				"textStyle": {
					"color": "white"
				},
				"gridlines": {
					"color": "transparent"
				},
			},
			"hAxis": {
				"textStyle": {
					"color": "white"
				},
				"gridlines": {
					"color": "transparent"
				},
			}
		},
		"formatters": {}
	};

	$scope.setChartForId = function (id) {
		$scope.chartObject.data.rows = [];

		for (var i = 0; i < 5; i++) {
			var month = moment().month(0-i).format("MMM YY");
			var c = {
				"c": [{
					"v": month
				},{
					"v": $scope.countTooks($scope.phones[id-1].takenBy, month)
				},{
					"v": $scope.countLocations($scope.phones[id-1].takenBy, month)
				},{
					"v": $scope.countPersons($scope.phones[id-1].takenBy, month)
				}]
			};
			$scope.chartObject.data.rows.push(c);
		}
	};

	$scope.countTooks = function (arr, month) {
		var count = 0;
		for (var i = 0; i < arr.length; i++) {
			if (
				(arr[i].timestamp_in !== undefined && moment(arr[i].timestamp_in).format("MMM YY") == month) || (arr[i].timestamp_out !== undefined && moment(arr[i].timestamp_out).format("MMM YY") == month)
			){
				count++;
			}
		}

		return count;
	};

	$scope.countLocations = function (arr, month) {
		var locations = {};
		for (var i = 0; i < arr.length; i++) {
			if (
				(arr[i].timestamp_in !== undefined && moment(arr[i].timestamp_in).format("MMM YY") == month) || (arr[i].timestamp_out !== undefined && moment(arr[i].timestamp_out).format("MMM YY") == month)
			){
				if (locations[arr[i].location] === undefined) {
					locations[arr[i].location] = 1;
				}
			}
		}

		return Object.keys(locations).length;
	};

	$scope.countPersons = function (arr, month) {
		var persons = {};
		for (var i = 0; i < arr.length; i++) {
			if (
				(arr[i].timestamp_in !== undefined && moment(arr[i].timestamp_in).format("MMM YY") == month) || (arr[i].timestamp_out !== undefined && moment(arr[i].timestamp_out).format("MMM YY") == month)
			){
				if (persons[arr[i].name] === undefined) {
					persons[arr[i].name] = 1;
				}
			}
		}

		return Object.keys(persons).length;
	};

	function sortObject(obj) {
		var arr = [];
		for (var prop in obj) {
			if (obj.hasOwnProperty(prop)) {
				arr.push({
					'name': prop,
					'loans': obj[prop]
				});
			}
		}
		arr.sort(function (a, b) {
			if (a.loans < b.loans) return 1;
			if (a.loans > b.loans) return -1;
			return 0;
		});

		return arr;
	}

	$scope.getPhones();

	// socket updates
	mySocket.on('update', function (data) {
		$scope.phones = data.phones;
	});

});


var createCookie = function(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
};

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}