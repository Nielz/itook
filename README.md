# iTook
## Installation
check out teh code, npm install and start server!
```sh
cd /YourProjectFolder/iTook
npm install
node server
```
Go to http://localhost:1337

##Usage
At first run you will get to fill in your name and location (eg Floor 4, desk 2, R&D, ...) This is stored with a cookie and used every time you visit the site, no passwords.
You can take a device and put it back. Updates are pushed to clients with socket.io.
A history of in and outgoing is kept per device. All data is stored in a JSON file. Edit JSON for adding extra devices. 