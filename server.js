var express = require('express'),
	bodyParser = require('body-parser'),
	fs = require('fs'),
	io = require('socket.io'),

	app = express(),
	jsonFile = __dirname + '/public/data/phones.json';

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public/'));

// make server
var server = app.listen(1337, function () {
	var host = server.address().address;
	var port = server.address().port;
	console.log('iTook live at http://%s:%s', host, port);
});

// make socket io
io = io.listen(server);
io.sockets.on('connection', function (socket) {
	socket.join(socket.handshake.sessionID);
});

var examplePhones = {
    "phones": [
        {
            "id": 1,
            "name": "Nokia Lumnia 900",
            "serialNumber": "",
            "img": "",
            "takenBy": [],
            "status": "available",
            "cable": "available"
        },
        {
            "id": 2,
            "name": "iPad 2",
            "serialNumber": "",
            "img": "/img/ipad2.png",
            "takenBy": [],
            "status": "available",
            "cable": "available"
        },
        {
            "id": 3,
            "name": "iPhone 4",
            "serialNumber": "",
            "img": "/img/iphone4.png",
            "takenBy": [],
            "status": "available",
            "cable": "available"
        },
        {
            "id": 4,
            "name": "iPhone 5C",
            "serialNumber": "",
            "img": "/img/iphone5c.png",
            "takenBy": [],
            "status": "available",
            "cable": "available"
        },
        {
            "id": 5,
            "name": "iPhone 5S",
            "serialNumber": "",
            "img": "/img/Iphone_5S.png",
            "takenBy": [],
            "status": "available",
            "cable": "available"
        },
        {
            "id": 6,
            "name": "Samsung SII",
            "serialNumber": "",
            "img": "/img/samsungs2.png",
            "takenBy": [],
            "status": "available",
            "cable": "available"
        },
        {
            "id": 7,
            "name": "Samsung S4",
            "serialNumber": "",
            "img": "/img/samsungs4.png",
            "takenBy": [
                {
                    "name": "John",
                    "location": "R&D",
                    "timestamp_out": 1415610196093
                }
            ],
            "status": "taken",
            "cable": "available"
        },
        {
            "id": 8,
            "name": "Acer",
            "serialNumber": "",
            "img": "",
            "takenBy": [],
            "status": "available",
            "cable": "available"
        },
        {
            "id": 9,
            "name": "Blackberry",
            "serialNumber": "",
            "img": "",
            "takenBy": [],
            "status": "available",
            "cable": "available"
        }
    ]
};

// make phones.json
if (!fs.existsSync(jsonFile)) {
	fs.writeFile(jsonFile, JSON.stringify(examplePhones), function (err) {
		if (err){
			console.log(err);
		}
	});
}

// when saved
app.post('/save', function(req, res){
	var json = {
		"phones": req.body
	};
	fs.writeFile(jsonFile, JSON.stringify(json, null, 4), function(err) {
		if (err) {
			console.log(err);
			res.end('oeps, er ging iets fout');
		} else {
			res.end('telefoons opgeslagen');
			io.sockets.in(req.sessionID).emit('update', json);
		}
	});
});